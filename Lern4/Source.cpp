#include<iostream>
#include <time.h>
const int N = 10;


void CreateArray(int Array[N][N])
{
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			Array[i][j] = i + j;
		}
	}
}

void printArray(int Array[N][N])
{
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			std::cout << Array[i][j] << '\t';
		}
		std::cout << '\n';
	}
}

int SumLineArray(int Line, int Array[N][N])
{
	int sum = 0;
	for (int i = 0; i < N; i++)
	{
		sum += Array[Line][i];
	}
	return sum;
}


int main()
{
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);

	int DataDay = buf.tm_mday;

	int array[N][N];
	int Line = DataDay % N;

	CreateArray(array);
	printArray(array);
	std::cout << "Summa Line = " << SumLineArray(Line, array);

}